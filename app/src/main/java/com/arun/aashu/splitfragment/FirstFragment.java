package com.arun.aashu.splitfragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    Button btn;

    View vie;
    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vie= inflater.inflate(R.layout.fragment_first, container, false);

         btn=  vie.findViewById(R.id.goButton);
        EditText ed1=vie.findViewById(R.id.writeHere);
       final String aa=ed1.getText().toString();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm=getFragmentManager();
               SecondFragment secondFragment= (SecondFragment) fm.findFragmentById(R.id.fragment2);
                 secondFragment.Jadu(aa);


                Toast.makeText(getActivity(), "Welcome to fragment", Toast.LENGTH_SHORT).show();
            }
        });



        // Inflate the layout for this fragment
        return  vie;
    }



}
