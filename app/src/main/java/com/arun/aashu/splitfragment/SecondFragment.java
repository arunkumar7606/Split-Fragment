package com.arun.aashu.splitfragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    View  vi;
    TextView t;

    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

          vi=inflater.inflate(R.layout.fragment_second, container, false);
             t=vi.findViewById(R.id.text2);



        // Inflate the layout for this fragment
        return  vi;
    }

    public void Jadu(String msg){


        t.setText(msg);
    }

}
